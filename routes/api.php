<?php

use Illuminate\Http\Request;

//route mobil
Route::get('mobil','C_mobil@mobil');
Route::get('mobil/{id_mobil}','C_mobil@mobilById');
Route::post('mobil','C_mobil@add')->middleware('auth:api');
Route::put('mobil/{mobil}','C_mobil@update')->middleware('auth:api');
Route::delete('mobil/{mobil}','C_mobil@delete')->middleware('auth:api');

//route promo
Route::get('promo','C_promo@promo');
Route::post('promo','C_promo@add')->middleware('auth:api');
Route::put('promo/{promo}','C_promo@update')->middleware('auth:api');
Route::delete('promo/{promo}','C_promo@delete')->middleware('auth:api');


//get user
Route::get('user','C_user@user')->middleware('auth:api');

//authentikasi login
Route::post('auth/register','AuthController@register');
Route::post('auth/login','AuthController@login');

//route wilayah
Route::get('provinsi','C_wilayah@provinsi');
Route::get('kab/{id_provinsi}','C_wilayah@kabByIdProvinsi');
Route::get('kec/{id_kab}','C_wilayah@kecByIdKab');
Route::get('kel/{id_kec}','C_wilayah@kelByIdKec');

Route::get('provinsi/{id_prov}','C_wilayah@provinsiById');
Route::get('kabupaten/{id_kab}','C_wilayah@kabById');
Route::get('kecamatan/{id_kec}','C_wilayah@kecById');
Route::get('kelurahan/{id_kel}','C_wilayah@kelById');


