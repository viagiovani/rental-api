<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_admin extends Model
{
    //
     protected $table = "t_admin";

     protected $fillable = [
        'username', 'password', 'nama','jabatan', 'api_token'
    ];
}
