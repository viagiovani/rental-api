<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_mobil extends Model
{
    protected $table = "t_mobil";

    protected $fillable = [
        'nama_mobil', 'merk', 'transmisi','warna', 'no_polisi','jumlah_kursi','tipe','harga_sewa'
    ];

    public $timestamps = false;

    protected $primaryKey = 'id_mobil';
}
