<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_promo extends Model
{
    protected $table = "t_promo";

    protected $fillable = [
        'nama_promo', 'deskripsi_promo', 'awal_berlaku','akhir_berlaku', 'id_mobil'
    ];

    public $timestamps = false;

    protected $primaryKey = 'id_promo';
}
