<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class AuthController extends Controller
{
    //
    public function register(Request $request, User $user){
        $this->validate($request,[
            'username'  => 'required',
            'email'  => 'required|email',
            'password'  => 'required|min:6',
            'name'      => 'required',

            
        ]);

        $user->create([
            'username'  => $request->username,
            'email'  => $request->email,
            'password'  => bcrypt($request->password),
            'name'      => $request->name,
            'api_token' => bcrypt($request->email),
            'role' => $request->role


        ]);

        return response()->json(['message' => 'Admin Berhasil Dibuat'],201);

    }

    public function login(Request $request, User $user){
        if (!Auth::attempt(['username' => $request->username, 'password' =>$request->password])) {
            return response()->json(['error' => 'Your Credential is wrong'],401);
        }

        $user = $user->find(Auth::user()->id);
        
        $data['api_token'] = $user->api_token;
        $data['role'] = $user->role;

        return response()->json($data,200);


    }
}
