<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\M_promo;
use App\Http\Requests;
use Auth;

class C_promo extends Controller
{
    public function promo(M_promo $promo){
        $hasil = $promo->all();

       return response()->json($hasil, 200);
    }

    public function add(Request $request, M_promo $promo){
        $promo->create([
            'nama_promo' => $request->nama_promo,
            'deskripsi_promo' => $request->deskripsi_promo,
            'awal_berlaku' => $request->awal_berlaku,
            'akhir_berlaku' => $request->akhir_berlaku,
            'id_mobil' => $request->id_mobil
        
        ]);

        return response()->json(['message' => 'Data promo berhasil ditambahkan'],201);

    }

    public function update(Request $request, M_promo $promo){
        $promo->nama_promo = $request->get('nama_promo', $promo->nama_promo);
        $promo->deskripsi_promo = $request->get('deskripsi_promo', $promo->deskripsi_promo);
        $promo->awal_berlaku = $request->get('awal_berlaku', $promo->awal_berlaku);
        $promo->akhir_berlaku = $request->get('akhir_berlaku', $promo->akhir_berlaku);
        $promo->id_mobil = $request->get('id_mobil', $promo->id_mobil);

        $promo->save();

    }

    public function delete(M_promo $promo){
        
        $promo->delete();

        return response()->json(['message'=>'Data has been deleted'],200);

    }
}
