<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\M_mobil;
use App\Http\Requests;
use App\Transformers\T_mobil;
use Auth;
use DB;
class C_mobil extends Controller
{
    public function mobil(M_mobil $mobil){
        $hasil = $mobil->all();
        

        return response()->json($hasil, 200);
    }

    public function add(Request $request, M_mobil $mobil){
        $mobil->create([
            'nama_mobil'  => $request->nama_mobil,
            'merk'  => $request->merk,
            'transmisi'  => $request->transmisi,
            'warna'      => $request->warna,
            'no_polisi' => $request->no_polisi,
            'jumlah_kursi' => $request->jumlah_kursi,
            'tipe' => $request->tipe,
            'harga_sewa' => $request->harga_sewa


        ]);

        return response()->json(['message' => 'Data mobil berhasil ditambahkan'],201);

    }

    public function mobilById(Request $request){
        
        $query = "SELECT * FROM t_mobil WHERE id_mobil = '".$request->id_mobil."'";

        $data = DB::select($query);

        return response()->json($data, 200);

        
    }

    public function update(Request $request, M_mobil $mobil){
        $mobil->nama_mobil = $request->get('nama_mobil', $mobil->nama_mobil);
        $mobil->merk = $request->get('merk', $mobil->merk);
        $mobil->transmisi = $request->get('transmisi', $mobil->transmisi);
        $mobil->warna = $request->get('warna', $mobil->warna);
        $mobil->no_polisi = $request->get('no_polisi', $mobil->no_polisi);
        $mobil->jumlah_kursi = $request->get('jumlah_kursi', $mobil->jumlah_kursi);
        $mobil->tipe = $request->get('tipe', $mobil->tipe);

        $mobil->save();

    }

    public function delete(M_mobil $mobil){
        
        $mobil->delete();

        return response()->json(['message'=>'Data has been deleted'],200);

    }
}
