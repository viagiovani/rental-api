<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\M_provinsi;
use DB;

class C_wilayah extends Controller
{
    //
    public function provinsi(Request $request){
        
        $query = "SELECT * FROM provinsi";

        $provinsi = DB::select($query);

        return response()->json($provinsi, 200);

        
    }

    public function provinsiById(Request $request){
        
        $query = "SELECT * FROM provinsi WHERE id_prov = '".$request->id_prov."'";

        $provinsi = DB::select($query);

        return response()->json($provinsi, 200);

        
    }

    public function kabById(Request $request){
        
        $query = "SELECT * FROM kabupaten WHERE id_kab = '".$request->id_kab."'";

        $data = DB::select($query);

        return response()->json($data, 200);

        
    }

    public function kecById(Request $request){
        
        $query = "SELECT * FROM kecamatan WHERE id_kec = '".$request->id_kec."'";

        $data = DB::select($query);

        return response()->json($data, 200);

        
    }

    public function kelById(Request $request){
        
        $query = "SELECT * FROM kelurahan WHERE id_kel = '".$request->id_kel."'";

        $data = DB::select($query);

        return response()->json($data, 200);

        
    }


     public function kabByIdProvinsi(Request $request){
        
        $query = "SELECT * FROM kabupaten WHERE id_prov = '".$request->id_provinsi."'";

        $data = DB::select($query);

        return response()->json($data, 200);

        
    }
    public function kecByIdKab(Request $request){
        
        $query = "SELECT * FROM kecamatan WHERE id_kab = '".$request->id_kab."'";

        $data = DB::select($query);

        return response()->json($data, 200);

        
    }

    public function kelByIdKec(Request $request){
        
        $query = "SELECT * FROM kelurahan WHERE id_kec = '".$request->id_kec."'";

        $data = DB::select($query);

        return response()->json($data, 200);

        
    }

    
}
