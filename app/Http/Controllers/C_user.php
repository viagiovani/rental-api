<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Auth;

class C_user extends Controller
{
    //

     public function user(User $user){
         $user = $user->find(Auth::user()->id);
        $hasil = $user->all();
        return response()->json($hasil);

        
    }

    public function profile(User $user){
        
        $user = $user->find(Auth::user()->id);

        return $user;
    }
}
