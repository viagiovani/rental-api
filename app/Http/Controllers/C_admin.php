<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\M_admin;
use App\Http\Requests;

class C_admin extends Controller
{
    //

     public function admin(M_admin $admin){
        $hasil = $admin->all();
        return response()->json($hasil);

        // return fractal()
        //     ->collection($hasil)
        //     ->transformWith(new T_admin)
        //     ->toArray();
    }
}
